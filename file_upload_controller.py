import ast
import json
import os
import numpy as np
import pandas as pd
from flask import request
from flask_jwt_extended import jwt_required
from flask_restful import Resource
from app.main.service.s3_service import S3Service


class FileUpload(Resource):
    @classmethod
    def post(cls):
        """
        Upload file to S3

        Returns:
            JSON
        """
        try:
            file = request.files.get('file')

            url = S3Service.upload(file)

            return {"url": url}, 200

        except Exception as e:
            return {"message": str(e)}, 422

class ReadLargeFileData(Resource):
    @classmethod
    @jwt_required
    def get(cls):
        """
        Read file from request and return the dataset in json format

        Returns:
            JSON
        """
        try:
            file_url = request.args.get('file_url')
            path_details = os.path.splitext(file_url)
            extension = path_details[1]
            count_row = 0
            if extension == '.csv':
                df = pd.read_csv(file_url, dtype = str, nrows=100, header=None, encoding="unicode_escape")

                new_df = pd.read_csv(file_url, dtype = str, encoding="unicode_escape")
                count_row = new_df.shape[0]
            else:
                df = pd.read_excel(file_url, dtype = str, nrows=100, header=None, encoding="unicode_escape")

                new_df = pd.read_excel(file_url, dtype = str, encoding="unicode_escape")
                count_row = new_df.shape[0]

            df = df.replace(np.nan, '', regex=True)
            lists = df.to_numpy().tolist()
            response = ast.literal_eval(json.dumps(lists))

            return {"data": response, "total_rows": count_row, "file_url": file_url}, 200
        except Exception as e:
            return {"message": str(e)}, 422